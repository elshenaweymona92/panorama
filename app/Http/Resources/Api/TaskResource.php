<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'project'               => $this->project->name,
            'task_name'             => $this->task_name,
            'status'                => $this->status,
            'created_at'            => $this->created_at,
    ];

    }
}
