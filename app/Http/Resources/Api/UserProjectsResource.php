<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class UserProjectsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $finish_tasks=$this->tasks()->whereStatus('finish');
       // dd($finish_tasks);
        return [
            'name'                => $this->name,
            'start_time'          => $this->start_time,
            'end_time'            => $this->end_time,
            'user_name'           =>  auth()->user()->name,
            'project_tasks'       =>  $this->tasks,
            'percentage_of_done_tasks'       =>  ($this->tasks()->whereStatus('finish')->count()/$this->tasks->count())*100 .'%'
        ];

    }
}
