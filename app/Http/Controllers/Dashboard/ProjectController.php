<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AddProjectRequest;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::with('user')->get();
        return view('dashboard.Projects.index', compact('projects'));
    }

    public function create()
    {
        $data['latest_projects'] = Project::orderBy('id', 'desc')->take(10)->get();
        return view('dashboard.Projects.create', $data);
    }

    public function store(AddProjectRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        $project = Project::create($data);
        return redirect()->route('projects.index')->with('success', 'add project successfully');
    }


    public function edit($id)
    {
        if (!Project::find($id)) {
            return redirect()->route('dashboard.project.index')->with('class', 'danger')->with('message', trans('dash.messages.try_2_access_not_found_content'));
        }
        $data['latest_projects'] = Project::orderBy('id', 'desc')->take(10)->get();
        $data['project'] = Project::find($id);

        return view('dashboard.Projects.edit', $data);
    }

    public function update(Request $request, Project $project)
    {
        $request_data = $request->all();
        $project->update($request_data);
        return redirect()->route('projects.index')->with('success', 'updated successfully');
    }



    public function show($id)
    {
        if (!Project::find($id)) {
            return redirect()->route('projects.index')->with('class', 'danger')->with('message', trans('dash.messages.try_2_access_not_found_content'));
        }
        $this->data['project'] = Project::where('id',$id)->with('tasks')->first();

       // $this->data['tasks'] = Task::where(['project_id' => $id])->get();
        return view('dashboard.Projects.show', $this->data);
    }




    public function delete(Request $request)
    {
        $user = Project::find($request->id);

        if (!$user) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, User is not exists !!']);
        try {
            $user->delete();
            return response()->json(['deleteStatus' => true, 'message' => 'تم الحذف  بنجاح']);
        } catch (Exception $e) {
            return response()->json(['deleteStatus' => false, 'error' => 'Server Internal Error 500']);
        }

        return redirect()->route('projects.index');
    }
}
