<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

;

class UserController extends Controller
{


    public function index()
    {
        $users = User::get();
        return view('dashboard.Users.index', compact('users'));
    }


    public function delete(Request $request)
    {
        // here we must validate user id and who can hae access for deleting this user.

        $user = User::find($request->id);

        if (!$user) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, User is not exists !!']);
        try {
            $user->delete();
            return response()->json(['deleteStatus' => true, 'message' => 'تم الحذف  بنجاح']);
        } catch (Exception $e) {
            return response()->json(['deleteStatus' => false, 'error' => 'Server Internal Error 500']);
        }

        return redirect()->route('users.index');
    }


}
