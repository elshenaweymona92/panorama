<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        return view('dashboard.auth.login');
    }


    public function adminLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $remember = isset($request['remember']) ? $request['remember'] : false;

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $remember)) {
            return redirect()->route('dashboard.home');
        }
        return back()->with('message', 'يرجى التأكد من صحة الايميل والباسورد.')->with('class', 'alert-danger');
    }


    public function logout()
    {
        Auth::guard('web')->logout();

        return redirect(route('dashboard.login'));
    }
}
