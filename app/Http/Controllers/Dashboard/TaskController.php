<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\createTaskRequest;
use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {

        $tasks = Task::with('project')->get();
        return view('dashboard.Tasks.index', compact('tasks'));
    }

    public function create()
    {
        $data['latest_tasks'] = Task::orderBy('id', 'desc')->take(10)->get();
        $data['projects'] = Project::all();
        return view('dashboard.Tasks.create', $data);
    }


    public function store(createTaskRequest $request)
    {
        $data = $request->all();
        $project = Task::create($data);
        return redirect()->route('tasks.index')->with('success', 'add project successfully');

    }


    public function edit($id)
    {
        if (!Task::find($id)) {
            return redirect()->route('tasks.index')->with('class', 'danger')->with('message', trans('dash.messages.try_2_access_not_found_content'));
        }
        $data['task'] = Task::find($id);

        return view('dashboard.Tasks.edit', $data);
    }

    public function update(Request $request, Task $task)
    {
        $request_data = $request->all();
        $task->update($request_data);
        return redirect()->route('tasks.index')->with('success', 'updated successfully');
    }


    public function delete(Request $request)
    {
        $user = Task::find($request->id);

        if (!$user) return response()->json(['deleteStatus' => false, 'error' => 'Sorry, User is not exists !!']);
        try {
            $user->delete();
            return response()->json(['deleteStatus' => true, 'message' => 'تم الحذف  بنجاح']);
        } catch (Exception $e) {
            return response()->json(['deleteStatus' => false, 'error' => 'Server Internal Error 500']);
        }

        return redirect()->route('tasks.index');
    }


}
