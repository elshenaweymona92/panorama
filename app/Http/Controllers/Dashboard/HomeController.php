<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\adminUpdateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    public function home()
    {
        $data['users_count'] = \App\Models\User::count();
        $data['projects_count'] = \App\Models\Project::count();
        $data['tasks_count'] = \App\Models\Task::count();


        return view('dashboard.index',$data);
    }






}
