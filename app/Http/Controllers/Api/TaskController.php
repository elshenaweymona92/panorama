<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\createTaskRequest;
use App\Http\Requests\Api\UpdateTaskRequest;
use App\Http\Resources\Api\TaskResource;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function create(createTaskRequest $request)
    {
        $data = $request->all();
        $task = Task::create($data);

        return response()->json(['status' => 'success', 'message' => 'added successfully', 'data' => new TaskResource($task)], 200); //CREATED
    }

    public function update(UpdateTaskRequest $request)
    {
        $task = Task::where(['id' => $request->task_id])->first();
        if (!$task)
            return response()->json(['status' => 'fails', 'message' => 'not found', 'data' => null], 400);//NOT_FOUND

        $task->update($request->validated());
        return response()->json(['status' => 'success', 'message' => 'updated successfully', 'data' => new TaskResource($task)], 200); //CREATED
    }

    public function delete(Request $request, $id)
    {
        $task = Task::find($id);
        if ($task) {
            $task->delete();
            return response()->json(['status' => 'success', 'message' => 'deleted-successfully', 'data' => ""], 200); //OK
        }
        return response()->json(['status' => 'fails', 'message' => 'there is no tasks.', 'data' => null], 403);//not found

    }

}
