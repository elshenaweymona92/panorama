<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\RegisterUserRequest;
use App\Http\Resources\Api\AuthResource;
use App\Models\Token;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{

    public function register(RegisterUserRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = User::create($request->all());

            $jwt_token = JWTAuth::fromUser($user);

            Token::create([
                'jwt' => $jwt_token,
                'user_id' => $user->id,
            ]);

            DB::commit();
            return responseJson('200', 'تم التسجيل بنجاح', new AuthResource($user)); //OK
        } catch (\Exception $e) {
            return responseJson('500', $e->getMessage());
        }
    }

    public function login(LoginRequest $request)
    {
        try {
            if (!$token = JWTAuth::attempt(['email' => $request->email, 'password' => $request->password])) {
                return responseJson('400', 'عفوا بيانات الدخول غير صحيحة', []);

            }
            auth()->user()->token->update(['jwt' => $token]);

            return responseJson('200', 'تم الدخول بنجاح', new AuthResource(auth()->user()));  //OK
        } catch (\Exception $e) {
            return responseJson('500', $e->getMessage(), []);
        }
    }


    public function showMyProfile()
    {
        return responseJson('200', 'user profile', new AuthResource(auth('api')->user()));  //OK

    }

    public function logout(Request $request)
    {
        auth()->user()->token->update(['jwt' => '']);
        auth()->logout();
        return responseJson('200', 'تم تسجيل الخروج', []); //OK
    }


}
