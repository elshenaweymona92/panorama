<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AddProjectRequest;
use App\Http\Requests\Api\UpdateProjectRequest;
use App\Http\Resources\Api\UserProjectsResource;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = Project::where('user_id', auth()->user()->id)->with('tasks')->get();

        return response()->json(['status' => 'success', 'message' => 'done-successfully', 'data' => UserProjectsResource::collection($projects)], 200); //ACCEPTED

    }

    public function create(AddProjectRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        $project = Project::create($data);

        return response()->json(['status' => 'success', 'message' => 'added successfully', 'data' => new UserProjectsResource($project)], 200); //CREATED
    }

    public function update(UpdateProjectRequest $request)
    {
        $project['user_id'] = auth()->user()->id;
        $project = Project::where(['id' => $request->project_id, 'user_id' => $request->user()->id])->first();
        if (!$project)
            return response()->json(['status' => 'fails', 'message' => 'not found', 'data' => null], 400);//NOT_FOUND

        $project->update($request->validated());
        return response()->json(['status' => 'success', 'message' => 'updated successfully', 'data' => new UserProjectsResource($project)], 200); //CREATED
    }

    public function delete(Request $request, $id)
    {
        $project = Project::find($id);
        if ($project) {

            if ($project->user_id == auth()->user()->id) {
                $project->delete();

                return response()->json(['status' => 'success', 'message' => 'deleted-successfully', 'data' => ""], 200); //OK
            }
            return response()->json(['status' => 'fails', 'message' => 'You do not have permission.', 'data' => null], 403);//FORBIDDEN
        }
        return response()->json(['status' => 'fails', 'message' => 'there is no projects.', 'data' => null], 403);//not found

    }


}
