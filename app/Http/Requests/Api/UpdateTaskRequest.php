<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\REQUEST_API_PARENT;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends REQUEST_API_PARENT
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'task_id'           =>'required|numeric|exists:tasks,id',
            'project_id'        =>'sometimes|numeric|exists:projects,id',
            'task_name'         =>'sometimes',
            'status'            => 'sometimes|in:on_beginning,on_progress,finish',
            // 'status'            =>'sometimes',

        ];
    }
}
