<?php


function responseJson($status, $message, $data = null)
{
    $response = [
        'status' => $status,
        'message' => $message,
        'data' => $data,
    ];
    return response()->json($response);

    function direction() { return app()->isLocale('ar') ? 'rtl' : 'ltr'; }
    function isNullable($text){ return (!isset($text) || $text == null || $text == '') ? trans('back.no-value') : ucwords($text); }


    function models($withColors = false)
    {
        if($withColors)
        {
            return [
                'teal'    => 'user',
//            'blue'    => 'user',
//            'green'   => 'admin',

            ];
        }
//    // those models for CRUD system only;
        return [
            'users2'      => 'user',


        ];
    }

    function getModelCount($model, $withDeleted = false)
    {
//    if($withDeleted)
//    {
//        if($model == 'user') return \App\Models\User::onlyTrashed()->where('type', 'admin')->count();
//        $mo = "App\\Models\\".ucwords($model);
//        return $mo::onlyTrashed()->count();
//    }

        if($model == 'user') return \App\Models\User::where('type', 'user')->count();
        if($model == 'admin') return \App\Models\User::where('type', 'admin')->count();
        if($model == 'representative') return \App\Models\User::where('type', 'representative')->count();


        $mo = "App\\Models\\".ucwords($model);

        return $mo::count();
    }



}




function random_colors()
{
    $color_array = [
        'slate-300', 'grey-300', 'brown-300', 'green-600', 'brown-600',
        'orange-300', 'orange-700', 'slate-700', 'green-300', 'teal-300',
        'blue-300', 'green-800', 'blue-600', 'blue-800', 'indigo-300', 'indigo-700',
        'purple-300', 'purple-600', 'violet-300', 'violet-600', 'pink-300', 'pink-600',
        'info-300', 'info-600', 'info-800', 'danger-300', 'danger-600'
    ];
    return $color_array[array_rand($color_array)];
}
