<?php

use App\Http\Controllers\Dashboard\AuthController;
use App\Http\Controllers\Dashboard\HomeController;
use App\Http\Controllers\Dashboard\ProjectController;
use App\Http\Controllers\Dashboard\TaskController;
use App\Http\Controllers\Dashboard\TasktController;
use App\Http\Controllers\Dashboard\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::group(['prefix' => 'dashboard',], function () {
    Route::get('/login', [AuthController::class, 'login'])->name('dashboard.login');
    Route::post('/login', [AuthController::class, 'adminLogin'])->name('dashboard.submit.login');
    Route::any('/logout', [AuthController::class, 'logout'])->name('dashboard.logout');

    // Route::group(['middleware' => 'auth'], function () {

    Route::get('home', [HomeController::class, 'home'])->name('dashboard.home');

    Route::resource('projects', ProjectController::class);
    Route::post('/ajax-delete-project', [ProjectController::class, 'delete'])->name('ajax-delete-project');

    Route::resource('tasks', TaskController::class);
    Route::post('/ajax-delete-tasks', [ProjectController::class, 'delete'])->name('ajax-delete-task');

//
    Route::resource('users', UserController::class);
    Route::post('/ajax-delete-user', [UserController::class, 'delete'])->name('ajax-delete-user');
    // });
});
