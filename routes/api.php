<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['namespace' => 'Api', 'prefix' => 'auth'], function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::any('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout'])->middleware('jwt.auth');

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('my_profile', [AuthController::class, 'showMyProfile']);

        Route::get('my_projects',     [ProjectController::class, 'index']);
        Route::post('create-project', [ProjectController::class, 'create']);
        Route::post('update-project', [ProjectController::class, 'update']);
        Route::post('delete-project/{id}', [ProjectController::class, 'delete']);

        Route::post('create-task', [TaskController::class, 'create']);
        Route::post('update-task', [TaskController::class, 'update']);
        Route::post('delete-task/{id}', [TaskController::class, 'delete']);




    });
});



