@extends('dashboard.layouts.master')

@section('title', trans('dash.home'))

@section('style')
<style>
    .custum-label{ font-size: 15px;border-color: cadetblue;background-color: cadetblue; }
</style>
@stop

@section('content')


    <!-- Page header -->
    @include('dashboard.layouts.header')

    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <div class="row">
            <div class="col-lg-3">
                <a href=''>
                    <div class="card bg-{{ random_colors() }}">
                        <div class="card-body">
                            <div class="d-flex">
                                <h3 class="font-weight-semibold mb-0">{{ $users_count }}</h3>
                            </div>
                            <div>
                                {{ trans('dash.users_count') }}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3">
                <a href="">
                    <div class="card bg-{{ random_colors() }}">
                        <div class="card-body">
                            <div class="d-flex">
                                <h3 class="font-weight-semibold mb-0">{{ $projects_count }}</h3>
                            </div>
                            <div>
                                {{ trans('dash.projects_count') }}
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3">
                <a href="">
                    <div class="card bg-{{ random_colors() }}">
                        <div class="card-body">
                            <div class="d-flex">
                                <h3 class="font-weight-semibold mb-0">{{$tasks_count}}</h3>
                            </div>
                            <div>
                                {{ trans('dash.tasks_count') }}

                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>


    </div>
    <!-- /content area -->
@stop

@section('scripts')

@stop
