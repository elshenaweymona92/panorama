@if(count($errors)>0)
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li> {{$error}} </li>
            @endforeach
        </ul>
    </div>
@endif



@if(session('success'))

    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>

        {{session('success')}}
    </div>

@endif



@if(session('error'))

    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>

        {{session('error')}}
    </div>

@endif
@if(session('message'))

    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
        {{session('message')}}
    </div>
@endif
