@if($setting->input == 0)
    <div class="form-group">
        <label for="{{$setting->key}}">{{ucwords($setting->name)}}</label>
        <input type="text" class="form-control form-data" name="{{$setting->key}}" id="{{$setting->key}}" value="{{$setting->value}}">
    </div>
@elseif($setting->input == 4)
    <div class="form-group">
        <div class="@error($setting->name) has-error @enderror">
            <div class="form-valid">
                <!-- CKEditor Container -->
                <label for="{{$setting->key}}">{{ ucwords($setting->name) }}</label>

                <textarea class="form-data" name="{{ $setting->key }}" id="{{$setting->key}}" rows="4" cols="4">{{$setting->value}}</textarea>

                @error($setting->name)<span><strong>{{ $message }}</strong></span>@enderror
            </div>
        </div>
    </div>
@else
    @if($setting->key == 'contact_site_location')
        <div class="form-group" id="setting_map">
            <div id="map-canvas" style="width: 100%; height: 300px;"></div>
            <input type="hidden" class="form-data" style="display: none;" id="map_settings_input" name="{{$setting->key}}" value="{{ $setting->value }}">
        </div>
    @else
        <div class="form-group">
            <label for="{{$setting->key}}">{{ucwords($setting->name)}}</label>
            <textarea class="form-control form-data" name="{{$setting->key}}" id="{{$setting->key}}" cols="30" rows="10">{{$setting->value}}</textarea>
        </div>
    @endif
@endif
