
@forelse($chunk as $i => $type)
    <div class="col-xs-12">
        <div class="panel panel-flat border-top-success">
            <div class="panel-heading">{{ucwords($type)}}</div>
            <div class="panel-body">
                @foreach($settings->where('type', $type) as $setting)
                    @include('dashboard.includes.settingsInput', ['setting' => $setting])
                    @if($setting->key == 'about_app')
                        <script>
                            CKEDITOR.replace('about_app', { height: '400px', extraPlugins: 'forms' });
                            CKEDITOR.instances.about_app.setData(`{!! isset($setting) ? $setting->value : '' !!}`);
                        </script>



                    @elseif($setting->key == 'app_policy')
                        <script>
                            CKEDITOR.replace('app_policy', { height: '400px', extraPlugins: 'forms' });
                            CKEDITOR.instances.app_policy.setData(`{!! isset($setting) ? $setting->value : '' !!}`);
                        </script>





                    @elseif($setting->key == 'app_terms')
                        <script>
                            CKEDITOR.replace('app_terms', { height: '300px', extraPlugins: 'forms' });
                            CKEDITOR.instances.app_terms.setData(`{!! isset($setting) ? $setting->value : '' !!}`);
                        </script>

                    @endif
                @endforeach
            </div>
        </div>
    </div>
@empty
    <div class="alert alert-info">@lang('back.no-var', ['var' => trans('back.settings')])</div>
@endforelse



















{{--@forelse($chunk as $i => $type)--}}
{{--    <div class="col-xs-12">--}}
{{--        <div class="panel panel-flat border-top-success">--}}
{{--            <div class="panel-heading">--}}
{{--                <h6 class="panel-title">{{ucwords($type)}}<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>--}}
{{--                <div class="heading-elements"><ul class="icons-list"><li><a data-action="collapse"></a></li></ul></div>--}}
{{--            </div>--}}
{{--            <div class="panel-body">--}}
{{--                @foreach($settings->where('type', $type) as $setting)--}}
{{--                    @include('dashboard.includes.settingsInput', ['setting' => $setting])--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@empty--}}
{{--    <div class="alert alert-info">لا يوجد إعدادات</div>--}}
{{--@endforelse--}}
