@extends('dashboard.layouts.master')

@section('title', trans('dash.home'))
@section('content')



    <body class="login-cover"
          style="background-image: url({{ asset('public/dashboard/assets/images/login_cover.jpg') }}); background-repeat: no-repeat; background-size: cover;">
    <!-- Content area -->


    <center>
        <div style="width: 420px;height: 520px; padding-top: 50px; ">

        @include('dashboard.layouts.partials.validation-errors')


        <!-- Simple login form -->
            <form method="POST" action="{{ route('dashboard.submit.login') }}">

                @csrf
                <div class="panel panel-body login-form" style=" box-shadow: 4px 2px 5px #aaa; border-radius:  20px;">
                    <div class="text-center">
                        <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                        {{--                    <img src="{{ asset('public/dashboard/assets/images/login_cover.jpg') }}" alt="logo"--}}
                        {{--                         style="width: 150px;height: 150px;">--}}

                        <h5 class="content-group">تسجيل الدخول
                            <small class="display-block">@lang('dash.admin-login-card')</small>
                        </h5>
                    </div>

                    <div class="form-group has-feedback has-feedback-left"
                         style=" box-shadow: 2px 2px 5px #aaa; border-radius:  20px;">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" placeholder="@lang('dash.email')"
                               autocomplete="email" autofocus>

                        <div class="form-control-feedback">
                            <i class="icon-user text-muted" style="padding-right: 10px;"></i>
                        </div>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                        	<strong style="color: red;">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group has-feedback has-feedback-left"
                         style=" box-shadow: 2px 2px 5px #aaa; border-radius:  20px;">
                        <input id="password" type="password"
                               class="form-control @error('password') is-invalid @enderror"
                               name="password" value="{{ old('password') }}" placeholder="@lang('dash.password')"
                               autocomplete="password" autofocus>

                        <div class="form-control-feedback">
                            <i class="icon-lock2 text-muted" style="padding-right: 10px; "></i>
                        </div>

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong style="color: red;">{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn bg-pink-400 btn-block">@lang('dash.login') <i
                                class="icon-circle-left2 position-right"></i></button>
                    </div>

                    {{-- 				<div class="text-center">
                                        <a href="login_password_recover.html">Forgot password?</a>
                                    </div> --}}
                </div>
            </form>
            <!-- /simple login form -->


        </div>
    </center>
    <!-- /content area -->
    </body>
@stop
