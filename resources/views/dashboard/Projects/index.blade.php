@extends('dashboard.layouts.master')

@section('title', trans('back.projects'))

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.dashboard')</span> - @lang('back.projects')
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a>
                </li>
                <li class="active">@lang('back.projects')</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->


    @include('dashboard.includes.errors')

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="" style="margin: 20px;">
        <div class="panel-heading">
            @include('dashboard.includes.table-header', ['collection' => $projects, 'name' => 'projects', 'icon' => 'projects'])
        </div>
        <br>
        <div class="list-icons" style="padding-right: 10px;">
            <a href="{{ route('projects.create') }}" class="btn btn-success btn-labeled btn-labeled-left"><b><i
                        class="icon-plus2"></i></b>اضافة مشروع جديد</a>
        </div>


        <table class="table datatable-basic" id="projects" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.form-name')</th>
                <th>start_time</th>
                <th>end_time</th>
                <th>creator</th>
                <th>@lang('back.since')</th>
                <th class="text-center">@lang('back.form-actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($projects as $project)
                <tr id="project-row-{{ $project->id }}">

                    <td>{{ $project->id }}</td>
                    <td><a href="#"> {{ ($project->name) }}</a></td>
                    <td>{{ $project->start_time }}</td>
                    <td>{{ $project->end_time }}</td>
                    <td>{{ $project->user->name }}</td>
                    <td>{{ $project->created_at->diffForHumans() }}</td>


                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                        class="icon-menu9"></i></a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{{ route('projects.edit',$project->id) }}"> <i
                                                class="icon-database-edit2"></i>@lang('back.edit') </a>
                                    </li>

                                    <li>
                                        <a href="{{ route('projects.show',$project->id) }}"> <i
                                                class="icon-database-edit2"></i>show tasks </a>
                                    </li>
                                        <li>
                                            <a data-id="{{ $project->id }}" class="delete-action"
                                               href="{{ Url('/project/project/'.$project->id) }}">
                                                <i class="icon-database-remove"></i>@lang('back.delete')
                                            </a>
                                        </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop

@section('scripts')
    @include('dashboard.layouts.ajax_delete', ['model' => 'project'])



@stop
