@extends('dashboard.layouts.master')

@section('title', trans('back.projects'))

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.dashboard')</span> - {{ trans('dash.project.show') }}
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a>
                </li>
                <li><a href="{{ url('/dashboard/projects') }}"><i
                            class="icon-home2 position-left"></i> @lang('dash.project.projects')</a>
                </li>
                <li class="active">{{ trans('dash.project.show') }}</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="" style="margin: 20px;">

        <div style="padding: 20px">
            <div><h4>name : {{ $project->name }} </h4></div>
            <div><h4>start_time : {{ $project->start_time }} </h4></div>
            <div><h4>end_time : {{ ($project->end_time) }}</h4></div>
            <div><h4>creator name : {{ ($project->user->name) }} </h4></div>
        </div>
        <hr>

        <table class="table datatable-basic" id="tasks" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.form-name')</th>
                <th>status</th>
                <th>project</th>
                <th>@lang('back.since')</th>
                <th class="text-center">@lang('back.form-actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($project->tasks as $task)
                <tr id="task-row-{{ $task->id }}">

                    <td>{{ $task->id }}</td>
                    <td><a href="#"> {{ ($task->task_name) }}</a></td>
                    <td>{{ $task->status }}</td>
                    <td>{{ $task->project->name }}</td>
                    <td>{{ $task->created_at->diffForHumans() }}</td>


                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                        class="icon-menu9"></i></a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{{ route('tasks.edit',$task->id) }}"> <i
                                                class="icon-database-edit2"></i>@lang('back.edit') </a>
                                    </li>
                                    <li>
                                        <a data-id="{{ $task->id }}" class="delete-action"
                                           href="{{ Url('/task/task/'.$task->id) }}">
                                            <i class="icon-database-remove"></i>@lang('back.delete')
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop

