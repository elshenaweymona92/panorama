@extends('dashboard.layouts.master')

@section('title', trans('back.create-var',['var'=>trans('back.project')]))


@section('content')


    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span>
                    - @lang('back.create-var',['var'=>trans('back.project')])
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/project') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li><a href="{{ route('projects.index') }}"><i
                            class="icon-projects position-left"></i> @lang('back.projects')
                    </a></li>
                <li class="active">@lang('back.create-var',['var'=>trans('back.project')])</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->



    @include('dashboard.includes.errors')

    <div class="row" style="padding: 15px;">
        <div class="col-md-6">

            <!-- Basic layout-->
            <form action="{{ route('projects.store') }}" class="form-horizontal" method="post"
                  enctype="multipart/form-data">
                @csrf
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"> اضافة مشروع جديد </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.name') }}</label>
                            <div class="col-lg-9">
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control"
                                       placeholder="{{ trans('dash.name') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.start_time') }}</label>
                            <div class="col-lg-9">
                                <input type="text" name="start_time" value="{{ old('start_time') }}"
                                       class="form-control"
                                       placeholder="{{ trans('dash.start_time') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.end_time') }}</label>
                            <div class="col-lg-9">
                                <input type="text" name="end_time" value="{{ old('end_time') }}" class="form-control"
                                       placeholder="{{ trans('dash.end_time') }}">
                            </div>
                        </div>


                        <div class="text-right">
                            <input type="submit" class="btn btn-primary"
                                   value=" {{ trans('dash.added_and_forward_to_list') }} "/>
                            {{--<input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.add_and_come_back') }} " />--}}
                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->

        </div>


        <div class="col-md-6">
            <div class="panel panel-flat">

                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('dash.latest_projects') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">

                    <table class="table table-bordered table-hover">
                        <tr class="text-center">
                            <th> {{ trans('dash.name') }} </th>
                        </tr>
                        @forelse($latest_projects as $project)
                            <tr>
                                <td> {{ $project->name }} </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="alert alert-info">no projects</td>
                            </tr>
                        @endforelse
                    </table>
                </div>

            </div>
        </div>
    </div>
@stop
