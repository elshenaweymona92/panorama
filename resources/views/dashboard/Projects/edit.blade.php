@extends('dashboard.layouts.master')

@section('title', trans('back.edit-var',['var'=>trans('back.project')]))

@section('content')
    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span>
                    - @lang('back.edit-var',['var'=>trans('back.project')])
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a>
                </li>
                <li><a href="{{ route('projects.index') }}"><i
                            class="icon-projects position-left"></i> @lang('back.projects')
                    </a></li>
                <li class="active">@lang('back.edit-var',['var'=>trans('back.project')])</li>

            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>

    @include('dashboard.includes.errors')


    <div class="row" style="padding-right: 20px;">
        <div class="col-md-6">

            <!-- Basic layout-->
            <form action="{{ route('projects.update',$project->id) }}" class="form-horizontal" method="post"
                  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"> {{ trans('dash.edit_data') }} </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.name') }}</label>
                            <div class="col-lg-9">
                                <input type="text" name="name" value="{{$project->name}}" class="form-control"
                                       placeholder="{{ trans('dash.name') }}">
                            </div>
                        </div>


                        <div class="text-right">
                            <input type="submit" class="btn btn-primary"
                                   value=" {{ trans('dash.update_and_forword_2_list') }} "/>
                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->
        </div>

    </div>



@stop
