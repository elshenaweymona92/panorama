<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ app()->isLocale('ar') ? 'rtl' : 'ltr'}}">

{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="{{ direction()}}">--}}
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="http-root" content="{{ url(Request::root()) }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script> window.laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!} </script>

    <title>@lang('dash.dashboard') || @yield('title')</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('dashboard/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('dashboard/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('dashboard/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('dashboard/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('dashboard/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <script src="https://kit.fontawesome.com/004184547c.js" crossorigin="anonymous"></script>
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('dashboard/assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('dashboard/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('dashboard/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('dashboard/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript"
            src="{{ asset('dashboard/assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/assets/js/pages/datatables_basic.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/assets/js/pages/components_modals.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/ckeditor/ckeditor.js') }}"></script>


    <script type="text/javascript" src="{{ asset('dashboard/assets/js/plugins/ui/ripple.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/assets/js/plugins/forms/inputs/touchspin.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/assets/js/pages/form_input_groups.js') }}"></script>


    <script type="text/javascript" src="{{ asset('dashboard/assets/js/pages/form_layouts.js') }}"></script>
    <script type="text/javascript"
            src="{{ asset('dashboard/assets/js/plugins/notifications/pnotify.min.js') }}"></script>

    <script type="text/javascript"
            src="{{ asset('dashboard/assets/js/pages/components_notifications_pnotify.js') }}"></script>
    <script type="text/javascript" src="{{ asset('dashboard/assets/js/pages/editor_ckeditor.js') }}"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>




@yield('style')
<!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
</head>
<body class="@yield('bodyclass')">

<div id="app">
    <div class="page-container">

        <div class="page-content">
            @if (!Request::is(app()->getLocale().'/dashboard/login'))

                <div class="sidebar sidebar-main sidebar-default">
                    <div class="sidebar-content">
                        <div class="sidebar-user-material">
                            <div class="category-content">
                                <div class="sidebar-user-material-content">
                                    <a href="javascript:void(0);">
{{--                                           <img src="{{Auth::guard('admin')->user()->ImagePath}}">--}}
{{--                                        <img src="{{Auth::guard('admin')->user()->ImagePath}}" class="img-circle img-responsive" alt="">--}}

{{--                                        <img src="{{ getImage('admins', isset($auth) ? $auth->ImagePath : null) }}" class="img-circle img-responsive" alt="">--}}
                                    </a>
{{--                                    <h6>{{Auth::guard('admin')->user()->full_name}}</h6>--}}
{{--                                    <span class="text-size-small">{{Auth::guard('admin')->user()->email}} </span>--}}
                                </div>

                                <div class="sidebar-user-material-menu">
                                    <a href="#user-nav" data-toggle="collapse"><span>@lang('dash.my-account')</span> <i
                                            class="caret"></i></a>
                                </div>
                            </div>

                            <div class="navigation-wrapper collapse" id="user-nav">
                                <ul class="navigation">
                                    <li>
                                        {{--                                        <a href="{{ route('show_profile') }}">--}}
                                        {{--                                            <i class="icon-user-plus"></i>--}}
                                        {{--                                            <span>@lang('dash.my-account')</span>--}}
                                        {{--                                        </a>--}}
                                    </li>
                                    <li>
                                        {{--                                        <a href="{{ route('dashboard.logout') }}"--}}
                                        {{--<a href=""--}}
                                        {{--                                           onclick="event.preventDefault();document.getElementById('logout-form-1').submit();">--}}
                                        {{--                                            <i class="icon-switch2"></i> @lang('dash.logout')--}}
                                        {{--                                    </a>--}}
                                        {{--                                        <form id="logout-form-1" action="{{ route('dashboard.logout') }}" method="post"--}}
                                        {{--                                              style="display: none;">--}}
                                        {{--                                            @csrf--}}
                                        {{--                                        </form>--}}

                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">

                                    <li class="navigation-header"><span>@lang('dash.main')</span>
                                        <i class="icon-menu" title="Main pages"></i>
                                    </li>

                                    @include('dashboard.layouts.sidebar')

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            @endif

            <div class="content-wrapper">
                @yield('content')

            </div>
        </div>
    </div>
</div>
<div id="site-modals"></div>

<script type="text/javascript" src="{{ url('assets/js/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ url('assets/js/bootstrap-datetimepicker.min.js') }}"
        charset="UTF-8"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
@yield('scripts')

<script type="text/javascript" src="{{ asset('assets/js/crud.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootbox.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/lang.min.js') }}"></script>

<script type="text/javascript"
        src="{{ asset('assets/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.printPage.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('assets/js/plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>
<script>
    function readURL(fileName) {
        if (fileName.files && fileName.files[0]) {
            var reader = new FileReader();
            reader.onload = (e) => {
                $('#viewImage').attr('src', e.target.result).width(90).height(90);
            };
            reader.readAsDataURL(fileName.files[0]);
        }
    }




        // ======== image preview ====== //
        $(".image").change(function() {

            if (this.files && this.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('.image-preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(this.files[0]);
            }

        });
</script>

<script src="{{ url('js/app.js') }}"></script>
<script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>
</body>
</html>
