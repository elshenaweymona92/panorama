<li>
{{--    <a href="{{route('dashboard.index')}}"><i class="icon-archive"></i> <span> {{ trans('dash.main_page') }} </span></a>--}}

</li>



<li>
<a href="#"><i class="icon-users"></i> <span> {{ trans('dash.users') }} </span></a>
    <ul>
        <li><a href="{{route('users.index')}}">{{ trans('dash.all') }}</a></li>
    </ul>
</li>

<li>
    <a href="#"><i class="icon-users"></i> <span> Projects </span></a>
    <ul>
        <li><a href="{{route('projects.index')}}">{{ trans('dash.all') }}</a></li>
        <li><a href="{{route('projects.create')}}"> {{ trans('dash.add') }}</a></li>
    </ul>
</li>

<li>
    <a href="#"><i class="icon-users"></i> <span> Tasks </span></a>
    <ul>
        <li><a href="{{route('tasks.index')}}">{{ trans('dash.all') }}</a></li>
        <li><a href="{{route('tasks.create')}}"> {{ trans('dash.add') }}</a></li>
    </ul>
</li>








