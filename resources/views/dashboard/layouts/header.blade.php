<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h1>
                <i class="icon-arrow-right6 position-left"></i>
                <img src="{{ asset('dashboard/assets/images/login_cover.jpg') }}" alt="aaaaa" style="width: 150px;height: 150px;">

             @lang('dash.dashboard')   الرئيسية
                {{--<span class="text-semibold">@lang('dash.home')</span> - @lang('dash.dashboard')--}}
            </h1>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb" style="float: right;">
            <li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i> @lang('dash.home')</a></li>
            <li class="active">@lang('dash.dashboard')</li>
        </ul>
        @include('dashboard.includes.quick-links')
    </div>
</div>
