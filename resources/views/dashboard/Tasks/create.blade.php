@extends('dashboard.layouts.master')

@section('title', trans('back.create-var',['var'=>trans('back.task')]))


@section('content')


    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.home')</span>
                    - @lang('back.create-var',['var'=>trans('back.task')])
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/task') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a></li>
                <li><a href="{{ route('tasks.index') }}"><i class="icon-tasks position-left"></i> @lang('back.tasks')
                    </a></li>
                <li class="active">@lang('back.create-var',['var'=>trans('back.task')])</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->



    @include('dashboard.includes.errors')

    <div class="row" style="padding: 15px;">
        <div class="col-md-6">

            <!-- Basic layout-->
            <form action="{{ route('tasks.store') }}" class="form-horizontal" method="post"
                  enctype="multipart/form-data">
                @csrf
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title"> اضافة تاسك جديد </h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">


                        <div class="form-group">
                            <label class="col-lg-3 control-label">name</label>
                            <div class="col-lg-9">
                                <input type="text" name="task_name" value="" class="form-control"
                                       placeholder="name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label
                                class="col-lg-3 control-label display-block"> {{ trans('dash.name') }} </label>
                            <div class="col-lg-9">
                                <select name="project_id" class="select-border-color border-warning form-control">

                                    @foreach ($projects as $project)
                                        <option value="{{ $project->id }}"> {{ $project->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">{{ trans('dash.status') }}</label>
                            <div class="col-lg-9">
                                <select name="status" class="select-border-color border-warning">
                                    <option value="on_beginning">on_beginning</option>
                                    <option value="on_progress">on_progress</option>
                                    <option value="finish">finish</option>
                                </select>
                            </div>
                        </div>


                        <div class="text-right">
                            <input type="submit" class="btn btn-primary"
                                   value=" {{ trans('dash.added_and_forward_to_list') }} "/>
                            {{--<input type="submit" class="btn btn-success" name="back" value=" {{ trans('dash.add_and_come_back') }} " />--}}
                        </div>
                    </div>
                </div>
            </form>
            <!-- /basic layout -->

        </div>


        <div class="col-md-6">
            <div class="panel panel-flat">

                <div class="panel-heading">
                    <h5 class="panel-title"> {{ trans('dash.latest_tasks') }} </h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">

                    <table class="table table-bordered table-hover">
                        <tr class="text-center">
                            <th> name</th>
                        </tr>
                        @forelse($latest_tasks as $task)
                            <tr>
                                <td> {{ $task->name }} </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="alert alert-info">no tasks</td>
                            </tr>
                        @endforelse
                    </table>
                </div>

            </div>
        </div>
    </div>
@stop
