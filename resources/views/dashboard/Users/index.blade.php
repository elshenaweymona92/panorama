@extends('dashboard.layouts.master')

@section('title', trans('back.users'))

@section('content')

    <!-- Page header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h4>
                    <i class="icon-arrow-right6 position-left"></i>
                    <span class="text-semibold">@lang('back.dashboard')</span> - @lang('back.users')
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('/dashboard') }}"><i class="icon-home2 position-left"></i> @lang('back.home')</a>
                </li>
                <li class="active">@lang('back.users')</li>
            </ul>

            @include('dashboard.includes.quick-links')
        </div>
    </div>
    <!-- /page header -->


    @include('dashboard.includes.errors')

    <!-- Basic datatable -->
    <div class="panel panel-flat" dir="" style="margin: 20px;">
        <div class="panel-heading">
            @include('dashboard.includes.table-header', ['collection' => $users, 'name' => 'users', 'icon' => 'users'])
        </div>


        <table class="table datatable-basic" id="users" style="font-size: 16px;">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('back.form-name')</th>
                <th>@lang('back.form-email')</th>
                <th>@lang('back.since')</th>
                <th class="text-center">@lang('back.form-actions')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr id="user-row-{{ $user->id }}">

                    <td>{{ $user->id }}</td>
                    <td><a href="#"> {{ ($user->name) }}</a></td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->created_at->diffForHumans() }}</td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                                        class="icon-menu9"></i></a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a data-id="{{ $user->id }}" class="delete-action"
                                           href="{{ Url('/user/user/'.$user->id) }}">
                                            <i class="icon-database-remove"></i>@lang('back.delete')
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /basic datatable -->
@stop

@section('scripts')
    @include('dashboard.layouts.ajax_delete', ['model' => 'user'])



@stop
