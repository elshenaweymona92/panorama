<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'            => 'يجب قبول حقل :attribute',
    'active_url'          => 'حقل :attribute لا يُمثّل رابطًا صحيحًا',
    'after'               => 'يجب على حقل :attribute أن يكون تاريخًا لاحقًا لحقل :date.',
    'after_or_equal'      => 'The :attribute must be a date after or equal to :date.',
    'alpha'               => 'يجب أن لا يحتوي حقل :attribute سوى على حروف',
    'alpha_dash'          => 'يجب أن لا يحتوي حقل :attribute على حروف، أرقام ومطّات.',
    'alpha_num'           => 'يجب أن يحتوي :attribute على حروفٍ وأرقامٍ فقط',
    'array'               => 'يجب أن يكون حقل :attribute ًمصفوفة',
    'before'              => 'يجب على حقل :attribute أن يكون تاريخًا سابقًا لحقل :date.',
    'before_or_equal'     => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'numeric' => 'يجب أن تكون قيمة :attribute محصورة ما بين :min و :max.',
        'file'    => 'يجب أن يكون حجم الملف :attribute محصورًا ما بين :min و :max كيلوبايت.',
        'string'  => 'يجب أن يكون عدد حروف النّص :attribute محصورًا ما بين :min و :max',
        'array'   => 'يجب أن يحتوي :attribute على عدد من العناصر محصورًا ما بين :min و :max',
    ],
    'boolean'            => 'يجب أن تكون قيمة حقل :attribute إما true أو false ',
    'confirmed'          => 'حقل التأكيد غير مُطابق للحقل :attribute',
    'date'               => 'حقل :attribute ليس تاريخًا صحيحًا',
    'date_equals'        => 'The :attribute must be a date equal to :date.',
    'date_format'        => 'لا يتوافق حقل :attribute مع الشكل :format.',
    'different'          => 'يجب أن يكون حقلان :attribute و :other مُختلفان',
    'digits'             => 'يجب أن يحتوي حقل :attribute على :digits رقمًا/أرقام',
    'digits_between'     => 'يجب أن يحتوي حقل :attribute ما بين :min و :max رقمًا/أرقام ',
    'dimensions'         => 'The :attribute has invalid image dimensions.',
    'distinct'           => 'The :attribute field has a duplicate value.',
    'email'              => 'حقل :attribute يجب ان يكون بريد الكتروني صالح',
    'ends_with'          => 'The :attribute must end with one of the following: :values',
    'exists'             => 'حقل :attribute غير  موجود',
    'file'               => 'The :attribute must be a file.',
    'filled'             => 'The :attribute field must have a value.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file'    => 'The :attribute must be greater than :value kilobytes.',
        'string'  => 'The :attribute must be greater than :value characters.',
        'array'   => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => 'The :attribute must be greater than or equal :value.',
        'file'    => 'The :attribute must be greater than or equal :value kilobytes.',
        'string'  => 'The :attribute must be greater than or equal :value characters.',
        'array'   => 'The :attribute must have :value items or more.',
    ],
    'image'              => 'حقل :attribute يجب ان يكون صورة',
    'in'                 => 'حقل :attribute غير صحيح',
    'in_array'           => 'The :attribute field does not exist in :other.',
    'integer'            => 'حقل :attribute يجب ان يكون رقمي صحيح',
    'ip'                 => 'The :attribute must be a valid IP address.',
    'ipv4'               => 'The :attribute must be a valid IPv4 address.',
    'ipv6'               => 'The :attribute must be a valid IPv6 address.',
    'json'               => 'The :attribute must be a valid JSON string.',
    'lt' => [
        'numeric' => 'The :attribute must be less than :value.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'string' => 'The :attribute must be less than :value characters.',
        'array' => 'The :attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => 'The :attribute must be less than or equal :value.',
        'file' => 'The :attribute must be less than or equal :value kilobytes.',
        'string' => 'The :attribute must be less than or equal :value characters.',
        'array' => 'The :attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file' => 'The :attribute may not be greater than :max kilobytes.',
        'string' => 'The :attribute may not be greater than :max characters.',
        'array' => 'The :attribute may not have more than :max items.',
    ],
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'numeric' => 'The :attribute must be at least :min.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'string' => 'The :attribute must be at least :min characters.',
        'array' => 'The :attribute must have at least :min items.',
    ],
    'not_in' => 'حقل :attribute يجب ان يكون بقيمة صحيحة',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'حقل :attribute يجب ان يكون رقمي',
    'present' => 'The :attribute field must be present.',
    'regex' => 'حقل :attribute بصيغة حاطئة',
    'required' => 'حقل :attribute مطلوب.',
    'required_if' => 'حقل :attribute مطلوب في حال ما إذا كان :other يساوي :value.',
    'required_unless' => 'حقل :attribute مطلوب في حال ما لم يكن :other يساوي :values.',
    'required_with' => 'حقل :attribute إذا توفّر :values.',
    'required_with_all' => 'حقل :attribute إذا توفّر :values.',
    'required_without' => 'حقل :attribute إذا لم يتوفّر :values.',
    'required_without_all' => 'حقل :attribute إذا لم يتوفّر :values.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'numeric' => 'The :attribute must be :size.',
        'file' => 'The :attribute must be :size kilobytes.',
        'string' => 'The :attribute must be :size characters.',
        'array' => 'The :attribute must contain :size items.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values',
    'string' => 'حقل :attribute يجب ان يكون نص',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique' => 'حقل :attribute موجود بالفعل',
    'uploaded' => 'The :attribute failed to upload.',
    'url' => 'حقل :attribute بصيغة غير صحيحة',
    'uuid' => 'The :attribute must be a valid UUID.',

    'mak_words' => 'حقل :attribute يجب ان يكون  رباعي',
    'youtube_link' => 'حقل :attribute يجب ان يكون رابط يوتيوب',
    'is_ar' => 'حقل :attribute يجب ان يكون باللغة العربية',
    'without_spaces' => 'حقل :attribute يجب ان لا يجتوي علي مسافات',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'video_link'                 => 'رابط الفيديو',
        'cost'                       => 'سعر الحجز',
        'image'                      => 'الصورة',
        'latitude'                   => 'خط الطول',
        'longitude'                  => 'خط العرض',
        'chairs'                     => 'عدد الكراسي',
        'ammount'                    => 'الكمية',
        'price'                      => 'السعر',
        'name'                       => 'الاسم',
        'email'                      => 'البريد الالكتروني',
        'password'                   => 'كلمة المرور',
        'phone'                      => 'رقم الهاتف',
        'commercial_registration_no' => 'رقم السجل التجاري',
        'shop_name'                  => 'اسم المحل',
        'shop_address'               => 'عنوان المحل',
        'date'                       => 'التاريخ',
        'neighborhood'               => 'الحي',
        'street'                     => 'الشارع',
        'bride_name'                 => 'اسم العروسة',
        'userData'                   => 'بيانات العضو',
        'userData.name'              => 'الاسم',
        'userData.email'             => 'البريد الالكتروني',
        'userData.phone'             => 'الهاتف',
        'userData.city_id'           => 'المدينة',
        'userData.bride_name'        => 'اسم العروسة',
        'selectedCategories.*.id'    => 'الفئة',
        'selectedCategories.*.name'  => 'اسم الفئة',

        'selectedCategories.*.selectedProviders.*.id'                       => 'رقم مقدم الخدمة',
        'selectedCategories.*.selectedProviders.*.shop_name'                => 'اسم المحل',
        'selectedCategories.*.selectedProviders.*.selectedProducts.*.id'    => 'رقم المنتج',
        'selectedCategories.*.selectedProviders.*.selectedProducts.*.name'  => 'اسم المنتج',
        'selectedCategories.*.selectedProviders.*.selectedProducts.*.price' => 'سعر المنتج',
        'selectedCategories.*.selectedProviders.*.selectedProducts.*.count' => 'كمية المنتج',
        'selectedCategories.*.selectedProviders.*.selectedProducts.*.total' => 'إجمالي التكلفة',

        'selectedCategories'         => 'التفاصيل',
        'reservation_date'           => 'تاريخ الحجز',
        'hall_id'                    => 'رقم القاعة',
        'to'                         => 'المرسل اليه',
        'message'                    => 'الرسالة',
        'subject'                    => 'عنوان الرسالة',
        'code'                       => 'الكود',
        'sms_code'                   => 'كود الرسالة',
        'city_id'                    => 'المدينة',
        'delegate_identity'          => 'المندوب',
        'start_time'                 => 'بداية دوام الفترة الاولي',
        'end_time'                   => 'نهاية دوام الفترة الاولي',
        'identity'                   => 'الهوية',
        'bank_name'                  => 'إسم البنك',
        'bank_account_no'            => 'رقم الحساب البنكي',
        'category_id'                => 'الفئة',
        'lat'                        => 'خط الطول',
        'long'                       => 'خط العرض',
        'market_id'                  => 'المحل',
        'rate'                       => 'التقيم',
        'comment'                    => 'التعليق',
        'ended_at'                   => 'تاريخ الانتهاء',
        'value'                      => 'القيمة',
        'second_start_time'          => 'بداية دوام الفترة الثانية',
        'second_end_time'            => 'نهاية دوام الفترة الثانية',
        'whatsapp'                   => 'رقم الوتس اب',
        'coupon_no'                  => 'الكوبون',
        'main_image_ar'              => 'الصورة الاساسية باللغة العربية',
        'main_image_en'              => 'الصورة الاساسية باللغة الانجليزية',
        'image_ar'                   => 'الصورة بالمحتوي العربي',
        'image_en'                   => 'الصورة بالمحتوي الانجليزي',
        'mobile'                     => 'رقم الجوال',
    ],
];
